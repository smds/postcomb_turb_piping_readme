The Turbine Inlet Piping assembly connects the output of the Post Combustor plenum to the input of the turbine. It consists of two pipe segments and three pipe fittings.

This sub-assembly is self-contained and does not require specifying any physical parameters.